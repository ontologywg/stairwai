Markdown documentation created by [pyLODE](http://github.com/rdflib/pyLODE) 2.12.0

# StairwAI AI Assets ontology

## Metadata
* **URI**
  * `http://purl.org/swai/spec#`
* **Version Information**
  * Creative Commons Attribution-NonCommercial-ShareAlike 2.0 Generic License
* **Imports**
  * [https://stairwai.gitlab.io/wp3/ontology/Country.ttl](https://stairwai.gitlab.io/wp3/ontology/Country.ttl)
  * [https://stairwai.gitlab.io/wp3/ontology/ai_onto.ttl](https://stairwai.gitlab.io/wp3/ontology/ai_onto.ttl)
  * [https://stairwai.gitlab.io/wp3/ontology/topics.ttl](https://stairwai.gitlab.io/wp3/ontology/topics.ttl)
* **Ontology RDF**
  * RDF ([StairwAI_AI_Assets_Ontology_slim.owl](xml))
### Description
Ontology developed for StairwAI project
html documentation: https://stairwai.gitlab.io/wp3/ontology/
Ontology purl: http://purl.org/swai/spec
Ontology real uri: https://stairwai.gitlab.io/wp3/ontology/StairwAI_AI_Assets_Ontology_slim.ttl

## Table of Contents
1. [Classes](#classes)
1. [Object Properties](#objectproperties)
1. [Functional Properties](#functionalproperties)
1. [Datatype Properties](#datatypeproperties)
1. [Annotation Properties](#annotationproperties)
1. [Named Individuals](#namedindividuals)
1. [Namespaces](#namespaces)
1. [Legend](#legend)


## Overview
![alt text](./images/StairwAI_ontology_diagram.png)
**Figure 1:** Ontology overview
## Classes
[esco:AwardingActivity](http://data.europa.eu/esco/model#AwardingActivity)
[esco:Qualification](http://data.europa.eu/esco/model#Qualification)
[esco:Skill](http://data.europa.eu/esco/model#Skill)
[AI Artifact](#AIArtifact)
[AI Asset](#AIAsset)
[AI expert](#AIexpert)
[AI Hardware Component](#AIHardwareComponent)
[Academic Resource](#AcademicResource)
[Agent](#Agent)
[Algorithm](#Algorithm)
[Benchmark](#Benchmark)
[Certification](#Certification)
[Container](#Container)
[Course](#Course)
[Dataset](#Dataset)
[Distribution](#Distribution)
[Education level](#Educationlevel)
[Education type](#Educationtype)
[Educational Resource](#EducationalResource)
[Environment](#Environment)
[Hardware Platform](#HardwarePlatform)
[Job Posting](#JobPosting)
[Language](#Language)
[Library](#Library)
[Measurement](#Measurement)
[Metric](#Metric)
[Model](#Model)
[Need](#Need)
[Open Call](#OpenCall)
[Organisation](#Organisation)
[Organisation Type](#OrganisationType)
[Pace](#Pace)
[Person](#Person)
[Post](#Post)
[Problem Statement](#ProblemStatement)
[Skill](#Skill1)
[Solution](#Solution)
[Tool](#Tool)
[saro:JobPosting](http://w3id.org/saro#JobPosting)
[saro:Sector](http://w3id.org/saro#Sector)
[ai:EducationalResource](http://www.ai4eu.eu/ontologies/core#EducationalResource)
[ai:HardwareComponent](http://www.ai4eu.eu/ontologies/core#HardwareComponent)
[ai:Library](http://www.ai4eu.eu/ontologies/core#Library)
[ai:Model](http://www.ai4eu.eu/ontologies/core#Model)
[ai:Publication](http://www.ai4eu.eu/ontologies/core#Publication)
[dcat:Dataset](http://www.w3.org/ns/dcat#Dataset)
[org:Organization](http://www.w3.org/ns/org#Organization)
[org:Post](http://www.w3.org/ns/org#Post)
[foaf1:Agent](http://xmlns.com/foaf/0.1/Agent)
[foaf1:Organization](http://xmlns.com/foaf/0.1/Organization)
[foaf1:Person](http://xmlns.com/foaf/0.1/Person)
[cry:Country](https://stairwai.gitlab.io/wp3/ontology/Country.ttl#Country)
[aicat:BusinessCategories](https://stairwai.gitlab.io/wp3/ontology/topics.ttl#BusinessCategories)
[aicat:TechnicalCategories](https://stairwai.gitlab.io/wp3/ontology/topics.ttl#TechnicalCategories)
### Awarding activity
Property | Value
--- | ---
URI | `http://data.europa.eu/esco/model#AwardingActivity`
Is Defined By | http://data.europa.eu/esco/model
Description | An awarding activity represents an activity related to the awarding of a qualification. It is used to specify an awarding body, a country or region where the qualification is awarded and optionally an awarding period.
### Qualification
Property | Value
--- | ---
URI | `http://data.europa.eu/esco/model#Qualification`
Is Defined By | http://data.europa.eu/esco/model
Description | A qualification is a formal outcome of an assessment and validation process, which is obtained when a competent body determines that an individual has achieved learning outcomes to given standards.
### Skill
Property | Value
--- | ---
URI | `http://data.europa.eu/esco/model#Skill`
Is Defined By | http://data.europa.eu/esco/model
Description | A skill may also be an informal recognition of a competence. The recognition typically is obtained by experience, practice or informal tests.
### AI Artifact
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#AIArtifact`
Description | An accessible, using a distribution, and usable artifact pertaining to AI research field.
Super-classes |[AI Asset](#AIAsset) (c)<br />
Restrictions |[executed in](#executedin) (op) **some** [Environment](#Environment) (c)<br />[owner](#owner) (op) **some** [Agent](#Agent) (c)<br />
Sub-classes |[Library](#Library) (c)<br />[Model](#Model) (c)<br />[Algorithm](#Algorithm) (c)<br />[Tool](#Tool) (c)<br />
In domain of |[doap:platform](http://usefulinc.com/ns/doap#platform) (dp)<br />[distributed as](#distributedas) (op)<br />[doap:programminglanguage](http://usefulinc.com/ns/doap#programminglanguage) (dp)<br />[applicable to](#applicableto) (op)<br />[doap:os](http://usefulinc.com/ns/doap#os) (dp)<br />[created by](#createdby) (op)<br />[wrapped in](#wrappedin) (op)<br />
In range of |[creator of](#creatorof) (op)<br />[has been applied in](#hasbeenappliedin) (op)<br />[wrapper of](#wrapperof) (op)<br />[distribution of](#distributionof) (op)<br />
### AI Asset
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#AIAsset`
Description | The AI-related elements that StairwAI offers to users.
Sub-classes |[AI Hardware Component](#AIHardwareComponent) (c)<br />[AI Artifact](#AIArtifact) (c)<br />[Job Posting](#JobPosting) (c)<br />[Person](#Person) (c)<br />[Academic Resource](#AcademicResource) (c)<br />[Course](#Course) (c)<br />[Dataset](#Dataset) (c)<br />[Benchmark](#Benchmark) (c)<br />
In domain of |[version](#version) (fp)<br />[doap:created](http://usefulinc.com/ns/doap#created) (fp)<br />[uuid identifier](#uuididentifier) (dp)<br />[license of distribution](#licenseofdistribution) (fp)<br />[doap:shortdesc](http://usefulinc.com/ns/doap#shortdesc) (dp)<br />[has language](#haslanguage) (op)<br />[Denotes the score of a AIAsset for a AI Technique](#DenotesthescoreofaAIAssetforaAITechnique) (op)<br />[is part of a solution](#ispartofasolution) (op)<br />[contact details](#contactdetails) (fp)<br />[doap:audience](http://usefulinc.com/ns/doap#audience) (fp)<br />[keyword](#keyword) (dp)<br />
In range of |[solution has part](#solutionhaspart) (op)<br />[score](#score) (op)<br />[used in](#usedin) (op)<br />
### AI expert
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#AIExpert`
Description | Person with expertise in the field of AI
Super-classes |[Person](#Person) (c)<br />
In domain of |[available expert](#availableexpert) (fp)<br />[team size](#teamsize) (dp)<br />[current organisation](#currentorganisation) (fp)<br />[expertise in](#expertisein) (op)<br />
In range of |[expertised by](#expertisedby) (op)<br />
### AI Hardware Component
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#AIHardwareComponent`
Description | A hardware AI resource (device/equipment).
Super-classes |[AI Asset](#AIAsset) (c)<br />
In domain of |[component has part](#componenthaspart) (op)<br />
In range of |[is part of a component](#ispartofacomponent) (op)<br />
### Academic Resource
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#AcademicResource`
Description | A published or potentially publishable, scholary resource.
Super-classes |[AI Asset](#AIAsset) (c)<br />
In domain of |[DOI (Digital Object Identifier)](#DOI(DigitalObjectIdentifier)) (fp)<br />[author](#author) (op)<br />[year of publication](#yearofpublication) (fp)<br />
In range of |[authored](#authored) (op)<br />
### Agent
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#Agent`
Description | Any entity (either persons, organizations, or software processes) having the capability to pro-actively act.
Restrictions |[email](#email) (dp) **min** 1<br />[linkedin url](#linkedinurl) (fp) **some** [xsd:anyURI](http://www.w3.org/2001/XMLSchema#anyURI) (c)<br />
Sub-classes |[Organisation](#Organisation) (c)<br />[Person](#Person) (c)<br />
In domain of |[org:location](http://www.w3.org/ns/org#location) (fp)<br />[owns](#owns) (op)<br />[resides](#resides) (op)<br />[generates statement](#generatesstatement) (op)<br />[email](#email) (dp)<br />[creator of](#creatorof) (op)<br />[linkedin url](#linkedinurl) (fp)<br />[participates in](#participatesin) (op)<br />
In range of |[statement generated by](#statementgeneratedby) (op)<br />[created by](#createdby) (op)<br />[owner](#owner) (op)<br />[has a participant](#hasaparticipant) (op)<br />[residence of](#residenceof) (op)<br />
### Algorithm
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#Algorithm`
Description | A recipe to perform a computational task, a finite sequence of instructions.
Super-classes |[AI Artifact](#AIArtifact) (c)<br />
In domain of |[generates](#generates) (op)<br />
In range of |[generated by](#generatedby) (op)<br />
### Benchmark
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#Benchmark`
Description | Evaluation by comparison with a standard metric.
Super-classes |[AI Asset](#AIAsset) (c)<br />
In domain of |[benchmark has part](#benchmarkhaspart) (op)<br />[evaluates](#evaluates) (op)<br />
In range of |[is part of a banchmark](#ispartofabanchmark) (op)<br />[evaluated with](#evaluatedwith) (op)<br />
### Certification
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#Certification`
Description | A document that certifies that a person has received specific education.
In domain of |[achieved Through](#achievedThrough) (op)<br />
In range of |[gives](#gives) (op)<br />
### Container
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#Container`
Description | A project that automates the deployment of applications within software containers, providing an additional layer of abstraction and application virtualization automation across multiple operating systems on top of a variety of locations, such as on-premises, in a public cloud, and/or in a private cloud.
Restrictions |[executed in](#executedin) (op) **some** [Environment](#Environment) (c)<br />
In domain of |[wrapper of](#wrapperof) (op)<br />
In range of |[wrapped in](#wrappedin) (op)<br />
Has members |[dockerContainer](http://purl.org/swai/spec#dockerContainer)<br />[jupyterNotebook](http://purl.org/swai/spec#jupyterNotebook)<br />
### Course
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#Course`
Description | A unit of teaching that typically lasts one academic term, is led by one or more instructors, and has a fixed roster of students.
Super-classes |[AI Asset](#AIAsset) (c)<br />
In domain of |[enrolled by](#enrolledby) (op)<br />[gives](#gives) (op)<br />[imparted in](#impartedin) (op)<br />[subject](#subject) (dp)<br />[credits](#credits) (fp)<br />[is paced as](#ispacedas) (op)<br />[imparted as](#impartedas) (op)<br />[has material](#hasmaterial) (op)<br />
In range of |[pace of](#paceof) (op)<br />[material of](#materialof) (op)<br />[enrols](#enrols) (op)<br />[achieved Through](#achievedThrough) (op)<br />
### Dataset
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#Dataset`
Description | A collection of data that is treated as a single unit by a computer
Super-classes |[AI Asset](#AIAsset) (c)<br />
Restrictions |[owner](#owner) (op) **some** [Agent](#Agent) (c)<br />
### Distribution
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#Distribution`
Description | A specific form of packaging and means of exposure and availability for an AI Artifact.
In domain of |[distribution of](#distributionof) (op)<br />
In range of |[distributed as](#distributedas) (op)<br />
Has members |[container](http://purl.org/swai/spec#container)<br />[saas](http://purl.org/swai/spec#saas)<br />[executable](http://purl.org/swai/spec#executable)<br />
### Education level
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#EducationLevel`
Description | Describe a standarization of the possible education level that a person can acheive
In range of |[has educational level](#haseducationallevel) (op)<br />
Has members |[Basic](http://purl.org/swai/spec#Basic)<br />[Bachelor](http://purl.org/swai/spec#Bachelor)<br />[Master](http://purl.org/swai/spec#Master)<br />[Advanced](http://purl.org/swai/spec#Advanced)<br />
### Education type
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#EducationType`
Description | The methodology used in order to teach a course
In range of |[imparted as](#impartedas) (op)<br />
Has members |[MOOC](http://purl.org/swai/spec#MOOC)<br />[BlendedLearning](http://purl.org/swai/spec#BlendedLearning)<br />[OnSite](http://purl.org/swai/spec#OnSite)<br />[Tutorial](http://purl.org/swai/spec#Tutorial)<br />[DistanceLearning](http://purl.org/swai/spec#DistanceLearning)<br />
### Educational Resource
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#EducationalResource`
Description | A resource built and distributed for educational purposes.
In domain of |[material of](#materialof) (op)<br />
In range of |[has material](#hasmaterial) (op)<br />
### Environment
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#Environment`
Description | The set of facilities, such as operating system, windows management, database, etc., that is available to a program when it is being executed.
In domain of |[runs on](#runson) (op)<br />
In range of |[executed in](#executedin) (op)<br />
### Hardware Platform
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#HardwarePlatform`
Description | A set of compatible hardware on which software applications can be run.
Restrictions |[owner](#owner) (op) **some** [Agent](#Agent) (c)<br />
In domain of |[is part of a component](#ispartofacomponent) (op)<br />
In range of |[runs on](#runson) (op)<br />[component has part](#componenthaspart) (op)<br />
### Job Posting
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#JobPosting`
Description | Primary means through which companies recruit new applicants for available positions.
Super-classes |[AI Asset](#AIAsset) (c)<br />
In domain of |[publication date](#publicationdate) (fp)<br />[desired technology](#desiredtechnology) (dp)<br />[preferences](#preferences) (fp)<br />[desirable skill](#desirableskill) (op)<br />[offered by](#offeredby) (op)<br />[have interest in](#haveinterestin) (op)<br />[required skill](#requiredskill) (op)<br />[was applicated by](#wasapplicatedby) (op)<br />[covers](#covers) (op)<br />[placed in](#placedin) (op)<br />
In range of |[receive interest of](#receiveinterestof) (op)<br />[covered by](#coveredby) (op)<br />[applies to](#appliesto) (op)<br />[offers](#offers) (op)<br />
### Language
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#Language`
Description | Class that maps the different langugages in Europe (adapted source:AI4EU platform)
In domain of |[used in](#usedin) (op)<br />
In range of |[has language](#haslanguage) (op)<br />
Has members |[Croatian](http://purl.org/swai/spec#Croatian)<br />[International](http://purl.org/swai/spec#International)<br />[Spanish](http://purl.org/swai/spec#Spanish)<br />[Swedish](http://purl.org/swai/spec#Swedish)<br />[Finnish](http://purl.org/swai/spec#Finnish)<br />[Maltese](http://purl.org/swai/spec#Maltese)<br />[Lithuanian](http://purl.org/swai/spec#Lithuanian)<br />[Dutch](http://purl.org/swai/spec#Dutch)<br />[French](http://purl.org/swai/spec#French)<br />[Latvian](http://purl.org/swai/spec#Latvian)<br />[Irish](http://purl.org/swai/spec#Irish)<br />[Czech](http://purl.org/swai/spec#Czech)<br />[German](http://purl.org/swai/spec#German)<br />[Polish](http://purl.org/swai/spec#Polish)<br />[Hungarian](http://purl.org/swai/spec#Hungarian)<br />[Greek](http://purl.org/swai/spec#Greek)<br />[Portuguese](http://purl.org/swai/spec#Portuguese)<br />[Italian](http://purl.org/swai/spec#Italian)<br />[Catalan](http://purl.org/swai/spec#Catalan)<br />[Romanian](http://purl.org/swai/spec#Romanian)<br />[English](http://purl.org/swai/spec#English)<br />[Slovenian](http://purl.org/swai/spec#Slovenian)<br />[Estonian](http://purl.org/swai/spec#Estonian)<br />[Slovak](http://purl.org/swai/spec#Slovak)<br />[Danish](http://purl.org/swai/spec#Danish)<br />[Bulgarian](http://purl.org/swai/spec#Bulgarian)<br />
### Library
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#Library`
Description | A suite of data and programming code that is used to develop software programs and applications, designed to assist both the programmer and the programming language compiler in building and executing software.
Super-classes |[AI Artifact](#AIArtifact) (c)<br />
### Measurement
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#Measurement`
Description | The size, length, or amount of something, in terms of a measure unit.
In domain of |[is part of a banchmark](#ispartofabanchmark) (op)<br />[has metric](#hasmetric) (op)<br />
In range of |[benchmark has part](#benchmarkhaspart) (op)<br />[metric of](#metricof) (op)<br />
### Metric
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#Metric`
Description | A system or standard of measurement.
In domain of |[metric of](#metricof) (op)<br />
In range of |[has metric](#hasmetric) (op)<br />
### Model
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#Model`
Description | A simplified representation of a system or a software process.
Super-classes |[AI Artifact](#AIArtifact) (c)<br />
In domain of |[generated by](#generatedby) (op)<br />
In range of |[generates](#generates) (op)<br />
### Need
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#Need`
Description | Something that is desired or required. It is distiled from a Problem Statement.
In domain of |[solved by](#solvedby) (op)<br />[appears in](#appearsin) (op)<br />
In range of |[has need](#hasneed) (op)<br />[solves](#solves) (op)<br />
### Open Call
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#OpenCall`
Description | A local, national or international competition open to SMEs, startups, scaleups, and mid-cap companies for the purpose of procuring the commissioning and provision of technology as part of the development.
In domain of |[has a participant](#hasaparticipant) (op)<br />[identifies](#identifies) (op)<br />[motivated by](#motivatedby) (op)<br />
In range of |[participates in](#participatesin) (op)<br />[motivates](#motivates) (op)<br />[identified by](#identifiedby) (op)<br />
### Organisation
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#Organisation`
Description | An organized group of people with a particular purpose, such as a business or government department.
Super-classes |[Agent](#Agent) (c)<br />
In domain of |[operates in](#operatesin) (op)<br />[offers](#offers) (op)<br />
In range of |[offered by](#offeredby) (op)<br />
### Organisation Type
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#OrganisationType`
Description | Categorisation of different types of organisations.
In domain of |[organisation type of](#organisationtypeof) (op)<br />
In range of |[has organisation type](#hasorganisationtype) (op)<br />
Has members |[nonGovernamentalOrganisation](http://purl.org/swai/spec#nonGovernamentalOrganisation)<br />[largeCompany](http://purl.org/swai/spec#largeCompany)<br />[publicBody](http://purl.org/swai/spec#publicBody)<br />[researchOrganisation](http://purl.org/swai/spec#researchOrganisation)<br />[smeCompany](http://purl.org/swai/spec#smeCompany)<br />[consortium](http://purl.org/swai/spec#consortium)<br />[startupCompany](http://purl.org/swai/spec#startupCompany)<br />[digitalInnovationHub](http://purl.org/swai/spec#digitalInnovationHub)<br />
### Pace
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#Pace`
Description | Describes the different paces in which a course is taught.
In domain of |[pace of](#paceof) (op)<br />
In range of |[is paced as](#ispacedas) (op)<br />
Has members |[PartTime](http://purl.org/swai/spec#PartTime)<br />[SelfPaced](http://purl.org/swai/spec#SelfPaced)<br />[FullTime](http://purl.org/swai/spec#FullTime)<br />
### Person
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#Person`
Description | People.
Super-classes |[AI Asset](#AIAsset) (c)<br />[Agent](#Agent) (c)<br />
Sub-classes |[AI expert](#AIexpert) (c)<br />
In domain of |[authored](#authored) (op)<br />[holds](#holds) (op)<br />[acquires](#acquires) (op)<br />[surname](#surname) (fp)<br />[applies to](#appliesto) (op)<br />[enrols](#enrols) (op)<br />
In range of |[held by](#heldby) (op)<br />[author](#author) (op)<br />[was applicated by](#wasapplicatedby) (op)<br />[acquired by](#acquiredby) (op)<br />[enrolled by](#enrolledby) (op)<br />
### Post
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#Post`
Description | A Post represents some position within an organization that exists independently of the person or persons filling it.
In domain of |[held by](#heldby) (op)<br />[covered by](#coveredby) (op)<br />
In range of |[holds](#holds) (op)<br />[covers](#covers) (op)<br />
### Problem Statement
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#ProblemStatement`
Description | Formal description of a problem that can be solved using AI.
In domain of |[motivation](#motivation) (fp)<br />[has need](#hasneed) (op)<br />[objective](#objective) (fp)<br />[statement generated by](#statementgeneratedby) (op)<br />[motivates](#motivates) (op)<br />[data availability](#dataavailability) (fp)<br />
In range of |[appears in](#appearsin) (op)<br />[motivated by](#motivatedby) (op)<br />[generates statement](#generatesstatement) (op)<br />
### Skill
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#Skill`
Description | The ability to do something well; expertise.
In domain of |[acquired by](#acquiredby) (op)<br />
In range of |[required skill](#requiredskill) (op)<br />[acquires](#acquires) (op)<br />[desirable skill](#desirableskill) (op)<br />
### Solution
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#Solution`
Description | The software that addresses an industry use case and is a fully executable application that can be deployed to the target hardware.
In domain of |[evaluated with](#evaluatedwith) (op)<br />[identified by](#identifiedby) (op)<br />[solution has part](#solutionhaspart) (op)<br />[solves](#solves) (op)<br />
In range of |[evaluates](#evaluates) (op)<br />[is part of a solution](#ispartofasolution) (op)<br />[solved by](#solvedby) (op)<br />[identifies](#identifies) (op)<br />
### Tool
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#Tool`
Description | A software that assists in activiities such as, and not limited to, data analysis, decision support, recommendation, etc.
Super-classes |[AI Artifact](#AIArtifact) (c)<br />
### Job Posting
Property | Value
--- | ---
URI | `http://w3id.org/saro#JobPosting`
Description | a listing that describes a job opening in a certain organization
### Sector
Property | Value
--- | ---
URI | `http://w3id.org/saro#Sector`
Description | The sector associated with the job position
### Educational Resource
Property | Value
--- | ---
URI | `http://www.ai4eu.eu/ontologies/core#EducationalResource`
Description | A resource built and distributed for educational purposes
### Hardware Component
Property | Value
--- | ---
URI | `http://www.ai4eu.eu/ontologies/core#HardwareComponent`
Description | A hardware AI resource (device/equipment).
### Library
Property | Value
--- | ---
URI | `http://www.ai4eu.eu/ontologies/core#Library`
Description | A distribution meant to be incorporated as a module into a larger framework or application.
### Model
Property | Value
--- | ---
URI | `http://www.ai4eu.eu/ontologies/core#Model`
Description | An AI Model.
### Publication
Property | Value
--- | ---
URI | `http://www.ai4eu.eu/ontologies/core#Publication`
Description | A published scholary manuscript.
### Dataset
Property | Value
--- | ---
URI | `http://www.w3.org/ns/dcat#Dataset`
Description | A collection of data, published or curated by a single source, and available for access or download in one or more represenations.
Scope Notes | The notion of dataset in DCAT is broad and inclusive, with the intention of accommodating resource types arising from all communities. Data comes in many forms including numbers, text, pixels, imagery, sound and other multi-media, and potentially other types, any of which might be collected into a dataset.
### Organization
Property | Value
--- | ---
URI | `http://www.w3.org/ns/org#Organization`
Description | Represents a collection of people organized together into a community or other social, commercial or political structure. The group has some common purpose or reason for existence which goes beyond the set of people belonging to it and can act as an Agent. Organizations are often decomposable into hierarchical structures.  It is recommended that SKOS lexical labels should be used to label the Organization. In particular `skos:prefLabel` for the primary (possibly legally recognized name), `skos:altLabel` for alternative names (trading names, colloquial names) and `skos:notation` to denote a code from a code list. Alternative names: _Collective_ _Body_ _Org_ _Group_
Super-classes |[foaf1:Agent](http://xmlns.com/foaf/0.1/Agent) (c)<br />
### Post
Property | Value
--- | ---
URI | `http://www.w3.org/ns/org#Post`
Description | A Post represents some position within an organization that exists independently of the person or persons filling it. Posts may be used to represent situations where a person is a member of an organization ex officio (for example the Secretary of State for Scotland is part of UK Cabinet by virtue of being Secretary of State for Scotland, not as an individual person). A post can be held by multiple people and hence can be treated as a organization in its own right.
### Agent
Property | Value
--- | ---
URI | `http://xmlns.com/foaf/0.1/Agent`
Description | An agent (eg. person, group, software or physical artifact).
Sub-classes |[foaf1:Organization](http://xmlns.com/foaf/0.1/Organization) (c)<br />[org:Organization](http://www.w3.org/ns/org#Organization) (c)<br />[foaf1:Person](http://xmlns.com/foaf/0.1/Person) (c)<br />
### Organization
Property | Value
--- | ---
URI | `http://xmlns.com/foaf/0.1/Organization`
Description | An organization.
Super-classes |[foaf1:Agent](http://xmlns.com/foaf/0.1/Agent) (c)<br />
### Person
Property | Value
--- | ---
URI | `http://xmlns.com/foaf/0.1/Person`
Description | A person.
Super-classes |[foaf1:Agent](http://xmlns.com/foaf/0.1/Agent) (c)<br />
### Country
Property | Value
--- | ---
URI | `https://stairwai.gitlab.io/wp3/ontology/Country.ttl#Country`
In domain of |[residence of](#residenceof) (op)<br />
In range of |[placed in](#placedin) (op)<br />[imparted in](#impartedin) (op)<br />[resides](#resides) (op)<br />
### BusinessCategories
Property | Value
--- | ---
URI | `https://stairwai.gitlab.io/wp3/ontology/topics.ttl#BusinessCategories`
In range of |[operates in](#operatesin) (op)<br />[related to](#relatedto) (op)<br />[applicable to](#applicableto) (op)<br />[experience in](#experiencein) (op)<br />
### TechnicalCategories
Property | Value
--- | ---
URI | `https://stairwai.gitlab.io/wp3/ontology/topics.ttl#TechnicalCategories`

## Object Properties
[achieved Through](#achievedThrough),
[acquired by](#acquiredby),
[acquires](#acquires),
[appears in](#appearsin),
[applicable to](#applicableto),
[applied on](#appliedon),
[applies to](#appliesto),
[apply](#apply),
[author](#author),
[authored](#authored),
[benchmark has part](#benchmarkhaspart),
[component has part](#componenthaspart),
[covered by](#coveredby),
[covers](#covers),
[created by](#createdby),
[creator of](#creatorof),
[desirable skill](#desirableskill),
[distributed as](#distributedas),
[distribution of](#distributionof),
[enrolled by](#enrolledby),
[enrols](#enrols),
[evaluated with](#evaluatedwith),
[evaluates](#evaluates),
[executed in](#executedin),
[experience in](#experiencein),
[expertise in](#expertisein),
[expertised by](#expertisedby),
[generated by](#generatedby),
[generates](#generates),
[generates statement](#generatesstatement),
[gives](#gives),
[has been applied in](#hasbeenappliedin),
[has educational level](#haseducationallevel),
[has language](#haslanguage),
[has material](#hasmaterial),
[has metric](#hasmetric),
[has need](#hasneed),
[has organisation type](#hasorganisationtype),
[has part](#haspart),
[has a participant](#hasaparticipant),
[have interest in](#haveinterestin),
[held by](#heldby),
[holds](#holds),
[identified by](#identifiedby),
[identifies](#identifies),
[imparted as](#impartedas),
[imparted in](#impartedin),
[is paced as](#ispacedas),
[is part of](#ispartof),
[is part of a banchmark](#ispartofabanchmark),
[is part of a component](#ispartofacomponent),
[is part of a solution](#ispartofasolution),
[material of](#materialof),
[metric of](#metricof),
[motivated by](#motivatedby),
[motivates](#motivates),
[offered by](#offeredby),
[offers](#offers),
[operates in](#operatesin),
[organisation type of](#organisationtypeof),
[owner](#owner),
[owns](#owns),
[pace of](#paceof),
[participates in](#participatesin),
[placed in](#placedin),
[receive interest of](#receiveinterestof),
[related to](#relatedto),
[required skill](#requiredskill),
[residence of](#residenceof),
[resides](#resides),
[runs on](#runson),
[score](#score),
[Denotes the score of a AIAsset for a AI Technique](#DenotesthescoreofaAIAssetforaAITechnique),
[solution has part](#solutionhaspart),
[solved by](#solvedby),
[solves](#solves),
[statement generated by](#statementgeneratedby),
[used in](#usedin),
[useful for](#usefulfor),
[was applicated by](#wasapplicatedby),
[wrapped in](#wrappedin),
[wrapper of](#wrapperof),
[](achievedThrough)
### achieved Through
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#achievedThrough`
Description | Denotes the giver of an official recognition.
Domain(s) |[Certification](#Certification) (c)<br />
Range(s) |[Course](http://purl.org/swai/spec#Course) (c)<br />
[](acquiredby)
### acquired by
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#acquiredBy`
Description | Denotes the person that adquire a specific skill
Domain(s) |[Skill](#Skill1) (c)<br />
Range(s) |[Person](http://purl.org/swai/spec#Person) (c)<br />
[](acquires)
### acquires
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#acquires`
Description | Someone learnt or developed a skill, habit, or quality.
Domain(s) |[Person](#Person) (c)<br />
Range(s) |[Skill](http://purl.org/swai/spec#Skill) (c)<br />
[](appearsin)
### appears in
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#appearsIn`
Description | Denotes the problem statements that have a specific need
Domain(s) |[Need](#Need) (c)<br />
Range(s) |[ProblemStatement](http://purl.org/swai/spec#ProblemStatement) (c)<br />
[](applicableto)
### applicable to
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#applicableTo`
Description | Defines the applicability that has an specific AI Artifact to a business area.
Domain(s) |[AI Artifact](#AIArtifact) (c)<br />
Range(s) |[aicat:BusinessCategories](https://stairwai.gitlab.io/wp3/ontology/topics.ttl#BusinessCategories) (c)<br />
[](appliedon)
### applied on
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#appliedOn`
Domain(s) |[aitec:AITechnique](https://stairwai.gitlab.io/wp3/ontology/ai_onto.ttl#AITechnique) (c)<br />
Range(s) |http://purl.org/swai/spec#AIHardwareComponent<br />http://purl.org/swai/spec#Dataset<br />
[](appliesto)
### applies to
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#appliesTo`
Description | Someone sent a formal request to be considered for a position or to be allowed to do or have something, submitted to an authority, institution, or organization.
Domain(s) |[Person](#Person) (c)<br />
Range(s) |[JobPosting](http://purl.org/swai/spec#JobPosting) (c)<br />
[](apply)
### apply
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#apply`
Description | Indicates that a Artifact apply a AI Technique
Domain(s) |([AI Artifact](#AIArtifact) (c) or [AI Hardware Component](#AIHardwareComponent) (c))<br />
Range(s) |[aitec:AITechnique](https://stairwai.gitlab.io/wp3/ontology/ai_onto.ttl#AITechnique) (c)<br />
[](author)
### author
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#author`
Description | Person that created an educational resource
Domain(s) |[Academic Resource](#AcademicResource) (c)<br />
Range(s) |[Person](http://purl.org/swai/spec#Person) (c)<br />
[](authored)
### authored
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#authored`
Description | Denotes the academic resources authored for a specific author
Domain(s) |[Person](#Person) (c)<br />
Range(s) |[AcademicResource](http://purl.org/swai/spec#AcademicResource) (c)<br />
[](benchmarkhaspart)
### benchmark has part
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#benchmarkHasPart`
Description | Indicates a measurement of a Benchmark.
Super-properties |[has part](#haspart) (op)<br />
Domain(s) |[Benchmark](#Benchmark) (c)<br />
Range(s) |[Measurement](http://purl.org/swai/spec#Measurement) (c)<br />
[](componenthaspart)
### component has part
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#componentHasPart`
Description | Indicates the platform that contains, as a part, a specific AI Hardware component
Super-properties |[has part](#haspart) (op)<br />
Domain(s) |[AI Hardware Component](#AIHardwareComponent) (c)<br />
Range(s) |[HardwarePlatform](http://purl.org/swai/spec#HardwarePlatform) (c)<br />
[](coveredby)
### covered by
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#coveredBy`
Description | Denotates the job posts that cover a specific post.
Domain(s) |[Post](#Post) (c)<br />
Range(s) |[JobPosting](http://purl.org/swai/spec#JobPosting) (c)<br />
[](covers)
### covers
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#covers`
Description | Indicates the available position.
Domain(s) |[Job Posting](#JobPosting) (c)<br />
Range(s) |[Post](http://purl.org/swai/spec#Post) (c)<br />
[](createdby)
### created by
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#createdBy`
Description | The person, or thing, that brings something into existence.
Domain(s) |[AI Artifact](#AIArtifact) (c)<br />
Range(s) |[Agent](http://purl.org/swai/spec#Agent) (c)<br />
[](creatorof)
### creator of
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#creatorOf`
Description | Denotates the artifacts that an Agent created
Domain(s) |[Agent](#Agent) (c)<br />
Range(s) |[AIArtifact](http://purl.org/swai/spec#AIArtifact) (c)<br />
[](desirableskill)
### desirable skill
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#desirableSkill`
Description | Indicates the desirable habilities.
Domain(s) |[Job Posting](#JobPosting) (c)<br />
Range(s) |[Skill](http://purl.org/swai/spec#Skill) (c)<br />
[](distributedas)
### distributed as
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#distributedAs`
Description | Indicates packaging conditions (licence, documentation, etc.).
Domain(s) |[AI Artifact](#AIArtifact) (c)<br />
Range(s) |[Distribution](http://purl.org/swai/spec#Distribution) (c)<br />
[](distributionof)
### distribution of
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#distributionOf`
Description | Indicates the AIArtifacts that are distributed under a specific distribution.
Domain(s) |[Distribution](#Distribution) (c)<br />
Range(s) |[AIArtifact](http://purl.org/swai/spec#AIArtifact) (c)<br />
[](enrolledby)
### enrolled by
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#enrolledBy`
Description | Indicates the persons that are enrolled in a specific course
Domain(s) |[Course](#Course) (c)<br />
Range(s) |[Person](http://purl.org/swai/spec#Person) (c)<br />
[](enrols)
### enrols
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#enrols`
Description | Someone officially registered as a student on a course.
Domain(s) |[Person](#Person) (c)<br />
Range(s) |[Course](http://purl.org/swai/spec#Course) (c)<br />
[](evaluatedwith)
### evaluated with
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#evaluatedWith`
Description | Indicates the benchmarks used to evaluate a Solution.
Domain(s) |[Solution](#Solution) (c)<br />
Range(s) |[Benchmark](http://purl.org/swai/spec#Benchmark) (c)<br />
[](evaluates)
### evaluates
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#evaluates`
Description | Indicates a judgement or assessment.
Domain(s) |[Benchmark](#Benchmark) (c)<br />
Range(s) |[Solution](http://purl.org/swai/spec#Solution) (c)<br />
[](executedin)
### executed in
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#executedIn`
Description | Indicates the place of performance of an instruction or program.
Domain(s) |([AI Artifact](#AIArtifact) (c) or [Container](#Container) (c))<br />
Range(s) |[Environment](http://purl.org/swai/spec#Environment) (c)<br />
[](experiencein)
### experience in
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#experienceIn`
Description | Indicates the experience that an expert has in, or the experience required for, a JobPosting in a business field.
Domain(s) |([AI expert](#AIexpert) (c) or [Job Posting](#JobPosting) (c))<br />
Range(s) |[aicat:BusinessCategories](https://stairwai.gitlab.io/wp3/ontology/topics.ttl#BusinessCategories) (c)<br />
[](expertisein)
### expertise in
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#expertiseIn`
Description | Indicate the AI Techniques that are expertised by a specific expert.
Domain(s) |[AI expert](#AIexpert) (c)<br />
Range(s) |[aitec:AITechnique](https://stairwai.gitlab.io/wp3/ontology/ai_onto.ttl#AITechnique) (c)<br />
[](expertisedby)
### expertised by
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#expertisedBy`
Description | Indicate the AI Experts that have expertise in a specific AI technique
Domain(s) |[aitec:AITechnique](https://stairwai.gitlab.io/wp3/ontology/ai_onto.ttl#AITechnique) (c)<br />
Range(s) |[AIExpert](http://purl.org/swai/spec#AIExpert) (c)<br />
[](generatedby)
### generated by
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#generatedBy`
Description | Indicates the algorithms used to generate a Model
Domain(s) |[Model](#Model) (c)<br />
Range(s) |[Algorithm](http://purl.org/swai/spec#Algorithm) (c)<br />
[](generates)
### generates
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#generates`
Description | Indicates the producer of a set or sequence of items by performing specified mathematical or logistical operations on an initial set.
Domain(s) |[Algorithm](#Algorithm) (c)<br />
Range(s) |[Model](http://purl.org/swai/spec#Model) (c)<br />
[](generatesstatement)
### generates statement
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#generatesStatement`
Description | An Agent that creates a new Problem Statement.
Domain(s) |[Agent](#Agent) (c)<br />
Range(s) |[ProblemStatement](http://purl.org/swai/spec#ProblemStatement) (c)<br />
[](gives)
### gives
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#gives`
Description | Indicates the Certifications that a Course gives to the pupils.
Domain(s) |[Course](#Course) (c)<br />
Range(s) |[Certification](http://purl.org/swai/spec#Certification) (c)<br />
[](hasbeenappliedin)
### has been applied in
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#hasBeenAppliedIn`
Description | Indicates some artifacts that applied a specific AI technique
Domain(s) |[aitec:AITechnique](https://stairwai.gitlab.io/wp3/ontology/ai_onto.ttl#AITechnique) (c)<br />
Range(s) |[AIArtifact](http://purl.org/swai/spec#AIArtifact) (c)<br />
[](haseducationallevel)
### has educational level
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#hasEducationLevel`
Description | Indicates the maximmum educational level acheived by a person or the educational level of a Course
Domain(s) |([AI expert](#AIexpert) (c) or [Course](#Course) (c) or [Job Posting](#JobPosting) (c))<br />
Range(s) |[EducationLevel](http://purl.org/swai/spec#EducationLevel) (c)<br />
[](haslanguage)
### has language
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#hasLanguage`
Description | Indicates a language used in an Asset
Domain(s) |[AI Asset](#AIAsset) (c)<br />
Range(s) |[Language](http://purl.org/swai/spec#Language) (c)<br />
[](hasmaterial)
### has material
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#hasMaterial`
Description | Indicates didactic resources offered in a course.
Domain(s) |[Course](#Course) (c)<br />
Range(s) |[EducationalResource](http://purl.org/swai/spec#EducationalResource) (c)<br />
[](hasmetric)
### has metric
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#hasMetric`
Description | Indicates an standard of measurement to be applied.
Domain(s) |[Measurement](#Measurement) (c)<br />
Range(s) |[Metric](http://purl.org/swai/spec#Metric) (c)<br />
[](hasneed)
### has need
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#hasNeed`
Description | Indicates an indispensable thing.
Domain(s) |[Problem Statement](#ProblemStatement) (c)<br />
Range(s) |[Need](http://purl.org/swai/spec#Need) (c)<br />
[](hasorganisationtype)
### has organisation type
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#hasOrganisationType`
Description | An Organisation type relation
Domain(s) |([AI expert](#AIexpert) (c) or [Job Posting](#JobPosting) (c) or [Organisation](#Organisation) (c))<br />
Range(s) |[OrganisationType](http://purl.org/swai/spec#OrganisationType) (c)<br />
[](haspart)
### has part
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#hasPart`
Description | Indicates the parts of an entity.
[](hasaparticipant)
### has a participant
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#hasParticipant`
Description | Indicates the participants of a Open Call
Domain(s) |[Open Call](#OpenCall) (c)<br />
Range(s) |[Agent](http://purl.org/swai/spec#Agent) (c)<br />
[](haveinterestin)
### have interest in
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#haveInterestIn`
Description | Indicates a technical area of interest for a Job post.
Domain(s) |[Job Posting](#JobPosting) (c)<br />
Range(s) |[aitec:AITechnique](https://stairwai.gitlab.io/wp3/ontology/ai_onto.ttl#AITechnique) (c)<br />
[](heldby)
### held by
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#heldBy`
Description | Indicates the person that holds a Post
Domain(s) |[Post](#Post) (c)<br />
Range(s) |[Person](http://purl.org/swai/spec#Person) (c)<br />
[](holds)
### holds
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#holds`
Description | Indicates a Post held by some Agent
Domain(s) |[Person](#Person) (c)<br />
Range(s) |[Post](http://purl.org/swai/spec#Post) (c)<br />
[](identifiedby)
### identified by
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#identifiedBy`
Description | Indicates the Open Call that generates a specific solution
Domain(s) |[Solution](#Solution) (c)<br />
Range(s) |[OpenCall](http://purl.org/swai/spec#OpenCall) (c)<br />
[](identifies)
### identifies
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#identifies`
Description | Indicates the results that solve the requirements.
Domain(s) |[Open Call](#OpenCall) (c)<br />
Range(s) |[Solution](http://purl.org/swai/spec#Solution) (c)<br />
[](impartedas)
### imparted as
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#impartedAs`
Description | Indicates the methodology used in a Course
Domain(s) |[Course](#Course) (c)<br />
Range(s) |[EducationType](http://purl.org/swai/spec#EducationType) (c)<br />
[](impartedin)
### imparted in
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#impartedIn`
Description | Indicates the Country in where a course is imparted
Domain(s) |[Course](#Course) (c)<br />
Range(s) |[cry:Country](https://stairwai.gitlab.io/wp3/ontology/Country.ttl#Country) (c)<br />
[](ispacedas)
### is paced as
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#isPacedAs`
Description | Describe in which manner is paced a course
Domain(s) |[Course](#Course) (c)<br />
Range(s) |[Pace](http://purl.org/swai/spec#Pace) (c)<br />
[](ispartof)
### is part of
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#isPartOf`
Description | Indicates the general entity that has another entity as its own part.
[](ispartofabanchmark)
### is part of a banchmark
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#isPartOfBenchmark`
Description | Indicates the measurements in a Benchmark.
Super-properties |[is part of](#ispartof) (op)<br />
Domain(s) |[Measurement](#Measurement) (c)<br />
Range(s) |[Benchmark](http://purl.org/swai/spec#Benchmark) (c)<br />
[](ispartofacomponent)
### is part of a component
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#isPartOfComponent`
Description | Indicates that a hardware platform is a part of a Hardware Component.
Super-properties |[is part of](#ispartof) (op)<br />
Domain(s) |[Hardware Platform](#HardwarePlatform) (c)<br />
Range(s) |[AIHardwareComponent](http://purl.org/swai/spec#AIHardwareComponent) (c)<br />
[](ispartofasolution)
### is part of a solution
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#isPartOfSolution`
Description | Indicates that a artifact, dataset or hardware platform is a part of a Solution.
Super-properties |[is part of](#ispartof) (op)<br />
Domain(s) |[AI Asset](#AIAsset) (c)<br />
Range(s) |[Solution](http://purl.org/swai/spec#Solution) (c)<br />
[](materialof)
### material of
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#materialOf`
Description | Indicates the Course that uses a specific educational resource
Domain(s) |[Educational Resource](#EducationalResource) (c)<br />
Range(s) |[Course](http://purl.org/swai/spec#Course) (c)<br />
[](metricof)
### metric of
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#metricOf`
Description | Indicates the measurements that uses a metric
Domain(s) |[Metric](#Metric) (c)<br />
Range(s) |[Measurement](http://purl.org/swai/spec#Measurement) (c)<br />
[](motivatedby)
### motivated by
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#motivatedBy`
Description | Indicates facts and arguments used in support of a something.
Domain(s) |[Open Call](#OpenCall) (c)<br />
Range(s) |[ProblemStatement](http://purl.org/swai/spec#ProblemStatement) (c)<br />
[](motivates)
### motivates
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#motivates`
Description | to cause someone to behave in a particular way
Domain(s) |[Problem Statement](#ProblemStatement) (c)<br />
Range(s) |[OpenCall](http://purl.org/swai/spec#OpenCall) (c)<br />
[](offeredby)
### offered by
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#offeredBy`
Description | Indicates the Organisation that offer a Jobposting
Domain(s) |[Job Posting](#JobPosting) (c)<br />
Range(s) |[Organisation](http://purl.org/swai/spec#Organisation) (c)<br />
[](offers)
### offers
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#offers`
Description | Indicates a job opportunity.
Domain(s) |[Organisation](#Organisation) (c)<br />
Range(s) |[JobPosting](http://purl.org/swai/spec#JobPosting) (c)<br />
[](operatesin)
### operates in
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#operatesIn`
Description | Indicates the area in which an organization develops its effort.
Domain(s) |[Organisation](#Organisation) (c)<br />
Range(s) |[aicat:BusinessCategories](https://stairwai.gitlab.io/wp3/ontology/topics.ttl#BusinessCategories) (c)<br />
[](organisationtypeof)
### organisation type of
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#organisationTypeOf`
Description | Indicates the experts, jobs offers and organisations related to a specific type of organisation
Domain(s) |[Organisation Type](#OrganisationType) (c)<br />
Range(s) |http://purl.org/swai/spec#AIExpert<br />http://purl.org/swai/spec#JobPosting<br />http://purl.org/swai/spec#Organisation<br />
[](owner)
### owner
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#owner`
Description | Proprietary of something.
Domain(s) |([AI Artifact](#AIArtifact) (c) or [Dataset](#Dataset) (c) or [Hardware Platform](#HardwarePlatform) (c))<br />
Range(s) |[Agent](http://purl.org/swai/spec#Agent) (c)<br />
[](owns)
### owns
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#owns`
Description | Indicates the items that an agent owns
Domain(s) |[Agent](#Agent) (c)<br />
Range(s) |http://purl.org/swai/spec#AIArtifact<br />http://purl.org/swai/spec#Dataset<br />http://purl.org/swai/spec#HardwarePlatform<br />
[](paceof)
### pace of
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#paceOf`
Description | Describe all the courses that are paced in a specific manner.
Domain(s) |[Pace](#Pace) (c)<br />
Range(s) |[Course](http://purl.org/swai/spec#Course) (c)<br />
[](participatesin)
### participates in
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#participatesIn`
Description | Indicates someone taking part in something.
Domain(s) |[Agent](#Agent) (c)<br />
Range(s) |[OpenCall](http://purl.org/swai/spec#OpenCall) (c)<br />
[](placedin)
### placed in
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#placedIn`
Description | Indicates the country where a jobpossting is placed
Domain(s) |[Job Posting](#JobPosting) (c)<br />
Range(s) |[cry:Country](https://stairwai.gitlab.io/wp3/ontology/Country.ttl#Country) (c)<br />
[](receiveinterestof)
### receive interest of
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#receiveInterestOf`
Description | Indicate the Job posts that have interest in a specific technique
Domain(s) |[aitec:AITechnique](https://stairwai.gitlab.io/wp3/ontology/ai_onto.ttl#AITechnique) (c)<br />
Range(s) |[JobPosting](http://purl.org/swai/spec#JobPosting) (c)<br />
[](relatedto)
### related to
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#relatedTo`
Description | Indicates economical area that a dataset belongs to
Domain(s) |([Course](#Course) (c) or [Dataset](#Dataset) (c))<br />
Range(s) |[aicat:BusinessCategories](https://stairwai.gitlab.io/wp3/ontology/topics.ttl#BusinessCategories) (c)<br />
[](requiredskill)
### required skill
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#requiredSkill`
Description | Indicates the habilities that must be fulfilled to the partaker to be considered as a candidate.
Domain(s) |[Job Posting](#JobPosting) (c)<br />
Range(s) |[Skill](http://purl.org/swai/spec#Skill) (c)<br />
[](residenceof)
### residence of
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#residenceOf`
Description | Indicates the agents that live in a country
Domain(s) |[cry:Country](https://stairwai.gitlab.io/wp3/ontology/Country.ttl#Country) (c)<br />
Range(s) |[Agent](http://purl.org/swai/spec#Agent) (c)<br />
[](resides)
### resides
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#resides`
Description | Indicates the country of origin of an Agent.
Domain(s) |[Agent](#Agent) (c)<br />
Range(s) |[cry:Country](https://stairwai.gitlab.io/wp3/ontology/Country.ttl#Country) (c)<br />
[](runson)
### runs on
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#runsOn`
Description | Indicates the execution platform
Domain(s) |[Environment](#Environment) (c)<br />
Range(s) |[HardwarePlatform](http://purl.org/swai/spec#HardwarePlatform) (c)<br />
[](score)
### score
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#score`
Description | Denotes the score of the AIAssets for specific AI Technique
Domain(s) |[aitec:AITechnique](https://stairwai.gitlab.io/wp3/ontology/ai_onto.ttl#AITechnique) (c)<br />
Range(s) |[AIAsset](http://purl.org/swai/spec#AIAsset) (c)<br />
[](DenotesthescoreofaAIAssetforaAITechnique)
### Denotes the score of a AIAsset for a AI Technique
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#scoredIn`
Domain(s) |[AI Asset](#AIAsset) (c)<br />
Range(s) |[aitec:AITechnique](https://stairwai.gitlab.io/wp3/ontology/ai_onto.ttl#AITechnique) (c)<br />
[](solutionhaspart)
### solution has part
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#solutionHasPart`
Description | Indicates the Solution that contains, as a part, a specific artifact, dataset or hardware platform.
Super-properties |[has part](#haspart) (op)<br />
Domain(s) |[Solution](#Solution) (c)<br />
Range(s) |[AIAsset](http://purl.org/swai/spec#AIAsset) (c)<br />
[](solvedby)
### solved by
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#solvedBy`
Description | Indicates the solutions that solve a need
Domain(s) |[Need](#Need) (c)<br />
Range(s) |[Solution](http://purl.org/swai/spec#Solution) (c)<br />
[](solves)
### solves
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#solves`
Description | Indicates something is a means of effectively dealing with a problem.
Domain(s) |[Solution](#Solution) (c)<br />
Range(s) |[Need](http://purl.org/swai/spec#Need) (c)<br />
[](statementgeneratedby)
### statement generated by
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#statementGeneratedBy`
Description | Indicates the agent that generates a problem statement
Domain(s) |[Problem Statement](#ProblemStatement) (c)<br />
Range(s) |[Agent](http://purl.org/swai/spec#Agent) (c)<br />
[](usedin)
### used in
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#usedIn`
Description | Describe the Assets that use a specific language
Domain(s) |[Language](#Language) (c)<br />
Range(s) |[AIAsset](http://purl.org/swai/spec#AIAsset) (c)<br />
[](usefulfor)
### useful for
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#usefulFor`
Description | Describe the relation that can exists between a Dataset or Hardware Component and the AI Techniques with which could be used.
Domain(s) |([AI Hardware Component](#AIHardwareComponent) (c) or [Dataset](#Dataset) (c))<br />
Range(s) |[aitec:AITechnique](https://stairwai.gitlab.io/wp3/ontology/ai_onto.ttl#AITechnique) (c)<br />
[](wasapplicatedby)
### was applicated by
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#wasApplicatedBy`
Description | Indicates the person that applies to a Job post
Domain(s) |[Job Posting](#JobPosting) (c)<br />
Range(s) |[Person](http://purl.org/swai/spec#Person) (c)<br />
[](wrappedin)
### wrapped in
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#wrappedIn`
Description | Indicates an enclosure.
Domain(s) |[AI Artifact](#AIArtifact) (c)<br />
Range(s) |[Container](http://purl.org/swai/spec#Container) (c)<br />
[](wrapperof)
### wrapper of
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#wrapperOf`
Description | Indicates the artifacts that are wrapped in a container
Domain(s) |[Container](#Container) (c)<br />
Range(s) |[AIArtifact](http://purl.org/swai/spec#AIArtifact) (c)<br />

## Functional Properties
[DOI (Digital Object Identifier)](#DOI(DigitalObjectIdentifier)),
[gdpr requirements](#gdprrequirements),
[available expert](#availableexpert),
[contact details](#contactdetails),
[credits](#credits),
[current organisation](#currentorganisation),
[data availability](#dataavailability),
[detailed description](#detaileddescription),
[identifier](#identifier),
[license of distribution](#licenseofdistribution),
[linkedin url](#linkedinurl),
[motivation](#motivation),
[name](#name),
[objective](#objective),
[preferences](#preferences),
[publication date](#publicationdate),
[requirements](#requirements),
[surname](#surname),
[trustworthy AI](#trustworthyAI),
[version](#version),
[year of publication](#yearofpublication),
[audience](#audience),
[created](#created),
[description](#description),
[location](#location),
[](DOI(DigitalObjectIdentifier))
### DOI (Digital Object Identifier)
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#DOI`
Description | Standard identifier of the academic resource
Domain(s) |[Academic Resource](#AcademicResource) (c)<br />
Range(s) |[xsd:string](http://www.w3.org/2001/XMLSchema#string) (c)<br />
[](gdprrequirements)
### gdpr requirements
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#GDPRrequirements`
Domain(s) |([AI Artifact](#AIArtifact) (c) or [Dataset](#Dataset) (c))<br />
Range(s) |[xsd:string](http://www.w3.org/2001/XMLSchema#string) (c)<br />
[](availableexpert)
### available expert
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#availableExpert`
Description | Indicates if an AI expert wiants to be discovered
Domain(s) |[AI expert](#AIexpert) (c)<br />
Range(s) |[xsd:boolean](http://www.w3.org/2001/XMLSchema#boolean) (c)<br />
[](contactdetails)
### contact details
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#contactDetails`
Description | Indicates the contact details of an Asset (e.g. email, contact person name, phone number)
Domain(s) |[AI Asset](#AIAsset) (c)<br />
Range(s) |[xsd:string](http://www.w3.org/2001/XMLSchema#string) (c)<br />
[](credits)
### credits
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#credits`
Description | Indicates the number of credits ects of a course
Domain(s) |[Course](#Course) (c)<br />
Range(s) |[xsd:integer](http://www.w3.org/2001/XMLSchema#integer) (c)<br />
[](currentorganisation)
### current organisation
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#currentOrganisation`
Description | Indicates the current organisation which an expert is currently working for.
Domain(s) |[AI expert](#AIexpert) (c)<br />
Range(s) |[xsd:string](http://www.w3.org/2001/XMLSchema#string) (c)<br />
[](dataavailability)
### data availability
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#dataAvailability`
Description | Description of the data provisioning of the problem statement
Domain(s) |[Problem Statement](#ProblemStatement) (c)<br />
Range(s) |[xsd:dateTime](http://www.w3.org/2001/XMLSchema#dateTime) (c)<br />
[](detaileddescription)
### detailed description
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#detailedDescription`
Description | Auxiliar information related to AI Asset.
Domain(s) |([AI Artifact](#AIArtifact) (c) or [Dataset](#Dataset) (c))<br />
Range(s) |[xsd:string](http://www.w3.org/2001/XMLSchema#string) (c)<br />
[](identifier)
### identifier
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#identifier`
Description | Sequence of ASCII characters that uniclly identify an individual.
Range(s) |[xsd:string](http://www.w3.org/2001/XMLSchema#string) (c)<br />
[](licenseofdistribution)
### license of distribution
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#licenseDistribution`
Domain(s) |[AI Asset](#AIAsset) (c)<br />
Range(s) |[xsd:string](http://www.w3.org/2001/XMLSchema#string) (c)<br />
[](linkedinurl)
### linkedin url
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#linkedinURL`
Description | Agents Linkedin link, to share the account.
Domain(s) |[Agent](#Agent) (c)<br />
Range(s) |[xsd:anyURI](http://www.w3.org/2001/XMLSchema#anyURI) (c)<br />
[](motivation)
### motivation
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#motivation`
Description | The motivation behind the statemement of the problem.
Domain(s) |[Problem Statement](#ProblemStatement) (c)<br />
Range(s) |[xsd:string](http://www.w3.org/2001/XMLSchema#string) (c)<br />
[](name)
### name
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#name`
Range(s) |[xsd:string](http://www.w3.org/2001/XMLSchema#string) (c)<br />
[](objective)
### objective
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#objective`
Description | The aim of the statemement of the problem.
Domain(s) |[Problem Statement](#ProblemStatement) (c)<br />
Range(s) |[xsd:string](http://www.w3.org/2001/XMLSchema#string) (c)<br />
[](preferences)
### preferences
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#preferences`
Description | Indicate the company's preferences in a job post
Domain(s) |[Job Posting](#JobPosting) (c)<br />
Range(s) |[xsd:string](http://www.w3.org/2001/XMLSchema#string) (c)<br />
[](publicationdate)
### publication date
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#publicationDate`
Description | Indicates he date that a Job post iwas publicated
Domain(s) |[Job Posting](#JobPosting) (c)<br />
Range(s) |[xsd:dateTime](http://www.w3.org/2001/XMLSchema#dateTime) (c)<br />
[](requirements)
### requirements
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#requirements`
Description | Indicates the requisites or requirements necessary for a task
Domain(s) |([Course](#Course) (c) or [Problem Statement](#ProblemStatement) (c))<br />
Range(s) |[xsd:string](http://www.w3.org/2001/XMLSchema#string) (c)<br />
[](surname)
### surname
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#surname`
Domain(s) |[Person](#Person) (c)<br />
Range(s) |[xsd:string](http://www.w3.org/2001/XMLSchema#string) (c)<br />
[](trustworthyAI)
### trustworthy AI
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#trustworthyAI`
Domain(s) |([AI Artifact](#AIArtifact) (c) or [Dataset](#Dataset) (c))<br />
Range(s) |[xsd:string](http://www.w3.org/2001/XMLSchema#string) (c)<br />
[](version)
### version
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#version`
Domain(s) |[AI Asset](#AIAsset) (c)<br />
Range(s) |[xsd:string](http://www.w3.org/2001/XMLSchema#string) (c)<br />
[](yearofpublication)
### year of publication
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#yearPublication`
Description | Academic resource's year of publication
Domain(s) |[Academic Resource](#AcademicResource) (c)<br />
Range(s) |[xsd:date](http://www.w3.org/2001/XMLSchema#date) (c)<br />
[](audience)
### audience
Property | Value
--- | ---
URI | `http://usefulinc.com/ns/doap#audience`
Description | Description of target user base
Domain(s) |[AI Asset](#AIAsset) (c)<br />
Range(s) |[rdfs:Literal](http://www.w3.org/2000/01/rdf-schema#Literal) (c)<br />
[](created)
### created
Property | Value
--- | ---
URI | `http://usefulinc.com/ns/doap#created`
Description | Date when something was created, in YYYY-MM-DD form. e.g. 2004-04-05
Domain(s) |[AI Asset](#AIAsset) (c)<br />
Range(s) |[xsd:date](http://www.w3.org/2001/XMLSchema#date) (c)<br />
[](description)
### description
Property | Value
--- | ---
URI | `http://usefulinc.com/ns/doap#description`
Domain(s) |([AI Asset](#AIAsset) (c) or [Problem Statement](#ProblemStatement) (c))<br />
Range(s) |[rdfs:Literal](http://www.w3.org/2000/01/rdf-schema#Literal) (c)<br />
[](location)
### location
Property | Value
--- | ---
URI | `http://www.w3.org/ns/org#location`
Description | Gives a location description for a person within the organization, for example a _Mail Stop_ for internal posting purposes.
Domain(s) |[Agent](#Agent) (c)<br />
Range(s) |[xsd:string](http://www.w3.org/2001/XMLSchema#string) (c)<br />

## Datatype Properties
[desired technology](#desiredtechnology),
[email](#email),
[keyword](#keyword),
[review comment](#reviewcomment),
[subject](#subject),
[team size](#teamsize),
[url](#url),
[uuid identifier](#uuididentifier),
[operating system](#operatingsystem),
[platform](#platform),
[programming language](#programminglanguage),
[short description](#shortdescription),
[](desiredtechnology)
### desired technology
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#desiredTechnology`
Description | Indiicates the technoologies that a company searches for a Job posting
Domain(s) |[Job Posting](#JobPosting) (c)<br />
Range(s) |[xsd:string](http://www.w3.org/2001/XMLSchema#string) (c)<br />
[](email)
### email
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#email`
Description | Email associated to an Agent
Domain(s) |[Agent](#Agent) (c)<br />
Range(s) |[xsd:string](http://www.w3.org/2001/XMLSchema#string) (c)<br />
[](keyword)
### keyword
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#keyword`
Description | Describing word of the area of application of an asset.
Domain(s) |[AI Asset](#AIAsset) (c)<br />
Range(s) |[xsd:string](http://www.w3.org/2001/XMLSchema#string) (c)<br />
[](reviewcomment)
### review comment
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#reviewComment`
Description | Comment left by another platform member on the item.
Domain(s) |([AI Artifact](#AIArtifact) (c) or [Academic Resource](#AcademicResource) (c) or [Dataset](#Dataset) (c))<br />
Range(s) |[xsd:string](http://www.w3.org/2001/XMLSchema#string) (c)<br />
[](subject)
### subject
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#subject`
Description | Indicates a subject, or subject category, of a course
Domain(s) |[Course](#Course) (c)<br />
Range(s) |[xsd:string](http://www.w3.org/2001/XMLSchema#string) (c)<br />
[](teamsize)
### team size
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#teamSize`
Domain(s) |[AI expert](#AIexpert) (c)<br />
Range(s) |[xsd:integer](http://www.w3.org/2001/XMLSchema#integer) (c)<br />
[](url)
### url
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#url`
Description | Link with information associated to a resource
Domain(s) |([AI Asset](#AIAsset) (c) or [Agent](#Agent) (c))<br />
Range(s) |[xsd:anyURI](http://www.w3.org/2001/XMLSchema#anyURI) (c)<br />
[](uuididentifier)
### uuid identifier
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#uuidIdentifier`
Description | UUID unique identifier for each asset instance
Domain(s) |[AI Asset](#AIAsset) (c)<br />
Range(s) |[xsd:string](http://www.w3.org/2001/XMLSchema#string) (c)<br />
[](operatingsystem)
### operating system
Property | Value
--- | ---
URI | `http://usefulinc.com/ns/doap#os`
Description | Operating system that a project is limited to.  Omit this property if the project is not OS-specific.
Domain(s) |[AI Artifact](#AIArtifact) (c)<br />
Range(s) |[rdfs:Literal](http://www.w3.org/2000/01/rdf-schema#Literal) (c)<br />
[](platform)
### platform
Property | Value
--- | ---
URI | `http://usefulinc.com/ns/doap#platform`
Description | Indicator of software platform (non-OS specific), e.g. Java, Firefox, ECMA CLR
Domain(s) |[AI Artifact](#AIArtifact) (c)<br />
Range(s) |[rdfs:Literal](http://www.w3.org/2000/01/rdf-schema#Literal) (c)<br />
[](programminglanguage)
### programming language
Property | Value
--- | ---
URI | `http://usefulinc.com/ns/doap#programminglanguage`
Description | Programming language a project is implemented in or intended for use with.
Domain(s) |[AI Artifact](#AIArtifact) (c)<br />
Range(s) |[rdfs:Literal](http://www.w3.org/2000/01/rdf-schema#Literal) (c)<br />
[](shortdescription)
### short description
Property | Value
--- | ---
URI | `http://usefulinc.com/ns/doap#shortdesc`
Description | Short (8 or 9 words) plain text description of a project.
Domain(s) |[AI Asset](#AIAsset) (c)<br />
Range(s) |[rdfs:Literal](http://www.w3.org/2000/01/rdf-schema#Literal) (c)<br />

## Annotation Properties
[issued](#issued),
[modified](#modified),
[alternative label](#alternativelabel),
[license](#license),
[ontology name](#ontologyname),
[direction](#direction),
[minCardinality](#minCardinality),
[term_status](#term_status),
[altLabel](#altLabel),
[changeNote](#changeNote),
[definition](#definition),
[editorialNote](#editorialNote),
[historyNote](#historyNote),
[scopeNote](#scopeNote),
[](issued)
### issued
Property | Value
--- | ---
URI | `http://purl.org/dc/terms/issued`
[](modified)
### modified
Property | Value
--- | ---
URI | `http://purl.org/dc/terms/modified`
[](alternativelabel)
### alternative label
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#alternativeLabel`
[](license)
### license
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#license`
[](ontologyname)
### ontology name
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#ontologyName`
[](direction)
### direction
Property | Value
--- | ---
URI | `http://www.w3.org/1999/02/22-rdf-syntax-ns#direction`
Description | The base direction component of a CompoundLiteral.
[](minCardinality)
### minCardinality
Property | Value
--- | ---
URI | `http://www.w3.org/2002/07/owl#minCardinality`
[](term_status)
### term_status
Property | Value
--- | ---
URI | `http://www.w3.org/2003/06/sw-vocab-status/ns#term_status`
[](altLabel)
### altLabel
Property | Value
--- | ---
URI | `http://www.w3.org/2004/02/skos/core#altLabel`
[](changeNote)
### changeNote
Property | Value
--- | ---
URI | `http://www.w3.org/2004/02/skos/core#changeNote`
[](definition)
### definition
Property | Value
--- | ---
URI | `http://www.w3.org/2004/02/skos/core#definition`
[](editorialNote)
### editorialNote
Property | Value
--- | ---
URI | `http://www.w3.org/2004/02/skos/core#editorialNote`
[](historyNote)
### historyNote
Property | Value
--- | ---
URI | `http://www.w3.org/2004/02/skos/core#historyNote`
[](scopeNote)
### scopeNote
Property | Value
--- | ---
URI | `http://www.w3.org/2004/02/skos/core#scopeNote`

## Named Individuals
[Advanced](#Advanced),
[Bachelor](#Bachelor),
[Basic](#Basic),
[Blended learning](#Blendedlearning),
[Bulgarian](#Bulgarian),
[Catalan](#Catalan),
[Consortium](#Consortium),
[Croatian](#Croatian),
[Czech](#Czech),
[Danish](#Danish),
[Digital and innovation hub](#Digitalandinnovationhub),
[Distance learning](#Distancelearning),
[Dutch](#Dutch),
[English](#English),
[Estonian](#Estonian),
[Executable](#Executable),
[Finnish](#Finnish),
[French](#French),
[Full-time](#Full-time),
[German](#German),
[Greek](#Greek),
[Hungarian](#Hungarian),
[International](#International),
[Irish](#Irish),
[Italian](#Italian),
[Large company](#Largecompany),
[Latvian](#Latvian),
[Lithunian](#Lithunian),
[MOOC](#MOOC),
[Maltese](#Maltese),
[Master](#Master),
[Non governamental organisation](#Nongovernamentalorganisation),
[On-site](#On-site),
[Part-time](#Part-time),
[Polish](#Polish),
[Portuguese](#Portuguese),
[Public body](#Publicbody),
[Research organisation](#Researchorganisation),
[Romanian](#Romanian),
[SAAS](#SAAS),
[SME company](#SMEcompany),
[Self-paced](#Self-paced),
[Slovak](#Slovak),
[Slovenian](#Slovenian),
[Spanish](#Spanish),
[Startup company](#Startupcompany),
[Swedish](#Swedish),
[Tutorial](#Tutorial),
[container](#container),
[docker container](#dockercontainer),
[jupyter notebook](#jupyternotebook),
### Advanced <sup>c</sup>
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#Advanced`
* **Class(es)**
  * [EducationLevel](http://purl.org/swai/spec#EducationLevel)
### Bachelor <sup>c</sup>
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#Bachelor`
* **Class(es)**
  * [EducationLevel](http://purl.org/swai/spec#EducationLevel)
### Basic <sup>c</sup>
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#Basic`
* **Class(es)**
  * [EducationLevel](http://purl.org/swai/spec#EducationLevel)
### Blended learning <sup>c</sup>
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#BlendedLearning`
* **Class(es)**
  * [EducationType](http://purl.org/swai/spec#EducationType)
### Bulgarian <sup>c</sup>
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#Bulgarian`
* **Class(es)**
  * [Language](http://purl.org/swai/spec#Language)
Description | bg
### Catalan <sup>c</sup>
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#Catalan`
* **Class(es)**
  * [Language](http://purl.org/swai/spec#Language)
Description | ca
### Croatian <sup>c</sup>
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#Croatian`
* **Class(es)**
  * [Language](http://purl.org/swai/spec#Language)
Description | hr
### Czech <sup>c</sup>
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#Czech`
* **Class(es)**
  * [Language](http://purl.org/swai/spec#Language)
Description | cs
### Danish <sup>c</sup>
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#Danish`
* **Class(es)**
  * [Language](http://purl.org/swai/spec#Language)
Description | da
### Distance learning <sup>c</sup>
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#DistanceLearning`
* **Class(es)**
  * [EducationType](http://purl.org/swai/spec#EducationType)
### Dutch <sup>c</sup>
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#Dutch`
* **Class(es)**
  * [Language](http://purl.org/swai/spec#Language)
Description | nl
### English <sup>c</sup>
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#English`
* **Class(es)**
  * [Language](http://purl.org/swai/spec#Language)
Description | en
### Estonian <sup>c</sup>
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#Estonian`
* **Class(es)**
  * [Language](http://purl.org/swai/spec#Language)
Description | et
### Finnish <sup>c</sup>
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#Finnish`
* **Class(es)**
  * [Language](http://purl.org/swai/spec#Language)
Description | fi
### French <sup>c</sup>
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#French`
* **Class(es)**
  * [Language](http://purl.org/swai/spec#Language)
Description | fr
### Full-time <sup>c</sup>
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#FullTime`
* **Class(es)**
  * [Pace](http://purl.org/swai/spec#Pace)
### German <sup>c</sup>
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#German`
* **Class(es)**
  * [Language](http://purl.org/swai/spec#Language)
Description | de
### Greek <sup>c</sup>
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#Greek`
* **Class(es)**
  * [Language](http://purl.org/swai/spec#Language)
Description | el
### Hungarian <sup>c</sup>
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#Hungarian`
* **Class(es)**
  * [Language](http://purl.org/swai/spec#Language)
Description | hu
### International <sup>c</sup>
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#International`
* **Class(es)**
  * [Language](http://purl.org/swai/spec#Language)
Description | Other language non described in the list.
### Irish <sup>c</sup>
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#Irish`
* **Class(es)**
  * [Language](http://purl.org/swai/spec#Language)
Description | ga
### Italian <sup>c</sup>
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#Italian`
* **Class(es)**
  * [Language](http://purl.org/swai/spec#Language)
Description | it
### Latvian <sup>c</sup>
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#Latvian`
* **Class(es)**
  * [Language](http://purl.org/swai/spec#Language)
Description | lv
### Lithunian <sup>c</sup>
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#Lithuanian`
* **Class(es)**
  * [Language](http://purl.org/swai/spec#Language)
Description | lt
### MOOC <sup>c</sup>
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#MOOC`
* **Class(es)**
  * [EducationType](http://purl.org/swai/spec#EducationType)
### Maltese <sup>c</sup>
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#Maltese`
* **Class(es)**
  * [Language](http://purl.org/swai/spec#Language)
Description | mt
### Master <sup>c</sup>
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#Master`
* **Class(es)**
  * [EducationLevel](http://purl.org/swai/spec#EducationLevel)
### On-site <sup>c</sup>
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#OnSite`
* **Class(es)**
  * [EducationType](http://purl.org/swai/spec#EducationType)
### Part-time <sup>c</sup>
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#PartTime`
* **Class(es)**
  * [Pace](http://purl.org/swai/spec#Pace)
### Polish <sup>c</sup>
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#Polish`
* **Class(es)**
  * [Language](http://purl.org/swai/spec#Language)
Description | pl
### Portuguese <sup>c</sup>
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#Portuguese`
* **Class(es)**
  * [Language](http://purl.org/swai/spec#Language)
Description | pt
### Romanian <sup>c</sup>
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#Romanian`
* **Class(es)**
  * [Language](http://purl.org/swai/spec#Language)
Description | ro
### Self-paced <sup>c</sup>
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#SelfPaced`
* **Class(es)**
  * [Pace](http://purl.org/swai/spec#Pace)
### Slovak <sup>c</sup>
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#Slovak`
* **Class(es)**
  * [Language](http://purl.org/swai/spec#Language)
Description | sk
### Slovenian <sup>c</sup>
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#Slovenian`
* **Class(es)**
  * [Language](http://purl.org/swai/spec#Language)
Description | sl
### Spanish <sup>c</sup>
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#Spanish`
* **Class(es)**
  * [Language](http://purl.org/swai/spec#Language)
Description | es
### Swedish <sup>c</sup>
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#Swedish`
* **Class(es)**
  * [Language](http://purl.org/swai/spec#Language)
Description | sv
### Tutorial <sup>c</sup>
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#Tutorial`
* **Class(es)**
  * [EducationType](http://purl.org/swai/spec#EducationType)
### Consortium <sup>c</sup>
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#consortium`
* **Class(es)**
  * [OrganisationType](http://purl.org/swai/spec#OrganisationType)
### container <sup>c</sup>
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#container`
* **Class(es)**
  * [Distribution](http://purl.org/swai/spec#Distribution)
### Digital and innovation hub <sup>c</sup>
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#digitalInnovationHub`
* **Class(es)**
  * [OrganisationType](http://purl.org/swai/spec#OrganisationType)
### docker container <sup>c</sup>
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#dockerContainer`
* **Class(es)**
  * [Container](http://purl.org/swai/spec#Container)
### Executable <sup>c</sup>
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#executable`
* **Class(es)**
  * [Distribution](http://purl.org/swai/spec#Distribution)
### jupyter notebook <sup>c</sup>
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#jupyterNotebook`
* **Class(es)**
  * [Container](http://purl.org/swai/spec#Container)
### Large company <sup>c</sup>
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#largeCompany`
* **Class(es)**
  * [OrganisationType](http://purl.org/swai/spec#OrganisationType)
### Non governamental organisation <sup>c</sup>
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#nonGovernamentalOrganisation`
* **Class(es)**
  * [OrganisationType](http://purl.org/swai/spec#OrganisationType)
### Public body <sup>c</sup>
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#publicBody`
* **Class(es)**
  * [OrganisationType](http://purl.org/swai/spec#OrganisationType)
### Research organisation <sup>c</sup>
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#researchOrganisation`
* **Class(es)**
  * [OrganisationType](http://purl.org/swai/spec#OrganisationType)
### SAAS <sup>c</sup>
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#saas`
* **Class(es)**
  * [Distribution](http://purl.org/swai/spec#Distribution)
### SME company <sup>c</sup>
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#smeCompany`
* **Class(es)**
  * [OrganisationType](http://purl.org/swai/spec#OrganisationType)
### Startup company <sup>c</sup>
Property | Value
--- | ---
URI | `http://purl.org/swai/spec#startupCompany`
* **Class(es)**
  * [OrganisationType](http://purl.org/swai/spec#OrganisationType)
## Namespaces
* **default (:)**
  * `http://purl.org/swai/spec#`
* **ai**
  * `http://www.ai4eu.eu/ontologies/core#`
* **aicat**
  * `https://stairwai.gitlab.io/wp3/ontology/topics.ttl#`
* **aitec**
  * `https://stairwai.gitlab.io/wp3/ontology/ai_onto.ttl#`
* **cry**
  * `https://stairwai.gitlab.io/wp3/ontology/Country.ttl#`
* **dc**
  * `http://purl.org/dc/elements/1.1/`
* **dcat**
  * `http://www.w3.org/ns/dcat#`
* **doap**
  * `http://usefulinc.com/ns/doap#`
* **esco**
  * `http://data.europa.eu/esco/model#`
* **fabio**
  * `http://purl.org/vocab/frbr/core#`
* **foaf**
  * `http://xmlns.com/foaf/spec/`
* **foaf1**
  * `http://xmlns.com/foaf/0.1/`
* **ns**
  * `http://www.w3.org/2003/06/sw-vocab-status/ns#`
* **org**
  * `http://www.w3.org/ns/org#`
* **owl**
  * `http://www.w3.org/2002/07/owl#`
* **prov**
  * `http://www.w3.org/ns/prov#`
* **rdf**
  * `http://www.w3.org/1999/02/22-rdf-syntax-ns#`
* **rdfs**
  * `http://www.w3.org/2000/01/rdf-schema#`
* **saro**
  * `http://w3id.org/saro#`
* **sdo**
  * `http://schema.org/`
* **skos**
  * `http://www.w3.org/2004/02/skos/core#`
* **terms**
  * `http://purl.org/dc/terms/`
* **xsd**
  * `http://www.w3.org/2001/XMLSchema#`

## Legend
* Classes: c
* Object Properties: op
* Functional Properties: fp
* Data Properties: dp
* Annotation Properties: dp
* Properties: p
* Named Individuals: ni
